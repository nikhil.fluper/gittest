package com.example.myapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.chethan.demoproject.DataRepository
import com.example.myapplication.model.EmoloyeeResponse

class DataViewModel(val dataRepository: DataRepository) : ViewModel() {
    var getdata = MutableLiveData<EmoloyeeResponse>()

    fun getAllEmployees(): LiveData<EmoloyeeResponse> = getdata

    fun getEmployees() {
        dataRepository.getProducts(object : DataRepository.OnProductData {

            override fun onSuccess(data: EmoloyeeResponse?) {

                getdata.value = data
            }

            override fun onFailure() {
                //REQUEST FAILED
            }
        })
    }
}