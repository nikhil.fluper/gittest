package com.example.myapplication.services

import com.example.myapplication.model.EmoloyeeResponse
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("api/v1/employees")
    fun getEmployeeData(): Call<EmoloyeeResponse>

}