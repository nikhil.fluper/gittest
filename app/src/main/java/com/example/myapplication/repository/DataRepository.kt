package com.chethan.demoproject

import com.example.myapplication.model.EmoloyeeResponse
import com.example.myapplication.services.ApiService
import retrofit2.Call
import retrofit2.Response

class DataRepository(val netWorkApi: ApiService) {

    fun getProducts(onProductData: OnProductData) {
        netWorkApi.getEmployeeData().enqueue(object : retrofit2.Callback<EmoloyeeResponse> {
            override fun onResponse(call: Call<EmoloyeeResponse>, response: Response<EmoloyeeResponse>) {
                onProductData.onSuccess(response.body())
            }

            override fun onFailure(call: Call<EmoloyeeResponse>, t: Throwable) {
                onProductData.onFailure()
            }
        })
    }

    interface OnProductData {
        fun onSuccess(data: EmoloyeeResponse?)
        fun onFailure()
    }
}

