package com.example.myapplication.application

import com.chethan.demoproject.DataRepository
import com.example.myapplication.services.ApiService
import com.example.myapplication.viewmodel.DataViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val mainModule = module {

    single { DataRepository(get()) }

    single { createWebService() }

    viewModel { DataViewModel(get()) }

}

fun createWebService(): ApiService {
    val retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl("http://dummy.restapiexample.com/")
        .build()

    return retrofit.create(ApiService::class.java)
}