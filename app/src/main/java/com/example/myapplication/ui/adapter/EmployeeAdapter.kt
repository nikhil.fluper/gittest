package com.example.myapplication.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.model.EmoloyeeResponse
import com.example.myapplication.model.Employee
import kotlinx.android.synthetic.main.item_layout.view.*

class EmployeeAdapter(val context: Context, private val emoloyeeResponse: EmoloyeeResponse) :
    RecyclerView.Adapter<EmployeeAdapter.EmployeeViewHolder>() {
    inner class EmployeeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var employee: Employee? = null
        var currentPosition: Int = 0


        init {
            itemView.setOnClickListener {
                Toast.makeText(context, employee!!.employee_name, Toast.LENGTH_SHORT).show()
            }
        }

        fun setData(skill: Employee, position: Int) {
            itemView.txvTitle.text = skill.employee_name
            this.employee = skill
            this.currentPosition = position

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeViewHolder {
        val itemView = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false)
        return EmployeeViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return emoloyeeResponse.data.size
    }

    override fun onBindViewHolder(holder: EmployeeViewHolder, position: Int) {
        val employee = emoloyeeResponse.data.get(position)
        holder.setData(employee, position)
    }
}