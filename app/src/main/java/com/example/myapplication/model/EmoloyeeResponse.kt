package com.example.myapplication.model

import com.example.myapplication.model.Employee
import java.io.Serializable

data class EmoloyeeResponse(

    val data: List<Employee>,
    val status: String?
) : Serializable

