package com.example.myapplication.model

import java.io.Serializable


class Employee(
    val id: String,
    val employee_name: String,
    val employee_salary: String,
    val employee_age: String,
    val profile_image: String
): Serializable